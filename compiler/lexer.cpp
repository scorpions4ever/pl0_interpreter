#include <cctype>
#include "lexer.h"
#include "keywords.h"
#include "pl0exception.h"

Lexer::Lexer(Scanner& scanner_obj) : scanner(scanner_obj)  {
    this->tokenType  = Symbols::nul;
    this->tokenValue = 0;
    this->tokenName  = "";
    this->ch         = ' ';
}

bool Lexer::getNextToken(void) {
    while (std::isspace(ch)) {
        ch = scanner.getChar();
    }

    if (std::isalpha(ch)) {
        // Identifier or reserved word
        tokenName = "";
        do {
            tokenName += ch;
            ch = scanner.getChar();
        } while (std::isalnum(ch));
        // Check if this is a recognized keyword. If not, this is an identifier
        auto word = keywords.find(tokenName);
        if (word == keywords.end()) {
            tokenType = Symbols::ident;
        }
        else {
            tokenType = word->second;
        }
        return true;
    }
    else if (std::isdigit(ch)) {
        std::string num_str = "";
        do {
            num_str += ch;
            ch = scanner.getChar();
        } while (std::isdigit(ch));

        sstrm.clear();
        sstrm.str(num_str);
        int tmp;
        sstrm >> tmp;
        if (sstrm.good() || sstrm.eof()) {
            tokenValue = tmp;
            tokenType  = Symbols::number;
            tokenName  = num_str; // Don't really need to do this.
            return true;
        }
        else {
            std::stringstream errorstrm;
            errorstrm << "Error with token '" << num_str << "' at position "
                      << scanner.getCurrentPos();
            throw PL0Exception(errorstrm.str());
        }
    }
    else if (ch == ':') {
        // Assignment operator
        ch = scanner.getChar();
        if (ch == '=') {
            tokenName = ":=";
            tokenType = Symbols::becomes;
            ch = scanner.getChar();
            return true;
        }
        else {
            tokenName = "";
            tokenType = Symbols::nul;
            // FIXME: Should we throw here instead? Wirth doesn't show error here.
            return true;
        }
    }
    else {
        tokenName = ch;
        auto token = symbols.find(tokenName);
        if (token != symbols.end()) {
            tokenType = token->second;
            ch = scanner.getChar();
            return true;
        }
        else {
            // FIXME: Again, handling this case isn't in Wirth's book. Could throw an exception here
            tokenName = "";
            tokenType = Symbols::nul;
            return true;
        }
    }

    return false;
}
