#pragma once

#include "symbols.h"
#include <unordered_map>
#include <string>

extern std::unordered_map<std::string, Symbols> keywords;
