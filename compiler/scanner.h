#pragma once

#include <string>

class Scanner {
 private:
    std::string buffer;
    std::string::size_type pos;
    
 public:
    Scanner(std::string input_file);
    const char getChar(void);
    std::string::size_type getCurrentPos();
};

