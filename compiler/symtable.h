#pragma once

#include <vector>
#include <string>
#include "objects.h"

struct SymtableEntry {
 public:
    std::string name;
    Objects kind;
    int level;
    int address;

    SymtableEntry(std::string name, Objects kind, int level, int address) {
	this->name = name;
	this->kind = kind;
	this->level = level;
	this->address = address;
    }
};

class SymTable {
 private:
    std::vector<SymtableEntry> table;
 public:
    void addSymbol(std::string name, Objects kind, int level, int *address);
    long findSymbol(std::string name);
    SymtableEntry get(long idx);
    auto getLastIndex(void) { return !table.empty() ? table.size() - 1 : 0; }
};
