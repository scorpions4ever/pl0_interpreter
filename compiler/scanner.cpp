#include <fstream>
#include <string>
#include "scanner.h"
#include "pl0exception.h"

using namespace std::string_literals;


Scanner::Scanner(std::string input_file) {
    std::ifstream infile(input_file);
    std::string line;
    while (std::getline(infile, line)) {
        buffer += line + "\n"s;
    }
    pos = 0;
}

const char Scanner::getChar(void) {
    if (pos >= buffer.length()) {
        throw PL0Exception("Program incomplete");
    }
    
    return buffer[pos++];
}

std::string::size_type Scanner::getCurrentPos() {
    return pos;
}
