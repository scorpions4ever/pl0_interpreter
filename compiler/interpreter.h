#pragma once

#include <vector>
#include <iostream>
#include "codelist.h"

class Interpreter {
 private:
    using Stack = std::vector<std::size_t>;
    const int STACKSIZE = 500;
    Stack stack;
    int programCounter;
    Stack::size_type baseRegister;
    Stack::size_type stackRegister;
    const CodeArray &codeListing;
    Stack::size_type baseAddress(int level);
 public:
    Interpreter(const CodeArray& codeListingObj);
    void doInterpret(std::ostream& os);
};
