#include "instruction.h"
#include "mnemonics.h"

Instruction::Instruction(Functions opCode, int level, int address) {
    this->opCode = opCode;
    this->level = level;
    this->address = address;
}

std::ostream& operator <<(std::ostream& os, const Instruction& obj) {
    auto item = mnemonics.find(obj.opCode);
    if (item != mnemonics.end()) {
	os << item->second << "\t" << obj.level << "\t" << obj.address;
    }
    else {
	os << "****** illegal op code *******"; // "cannot happen", as obj.opCode is an enum class and can't have values outside range
    }
    return os;
}
