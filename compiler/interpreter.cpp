#include "interpreter.h"

Interpreter::Interpreter(const CodeArray& codeListingObj) : codeListing(codeListingObj) {
    programCounter = 0;
    baseRegister = 1;
    stackRegister = 2;
    stack.resize(STACKSIZE);
    stack[0] = stack[1] = stack[2] = 0;
}

Interpreter::Stack::size_type Interpreter::baseAddress(int level) {
    auto tmpBaseRegister = baseRegister;
    while (level > 0) {
	tmpBaseRegister = stack[tmpBaseRegister];
	--level;
    }
    return tmpBaseRegister;
}

void Interpreter::doInterpret(std::ostream& os) {
    do {
	auto instruction = codeListing[programCounter];
	++programCounter;
	switch (instruction.opCode) {
	case Functions::lit:
	    ++stackRegister;
	    stack[stackRegister] = instruction.address;
	    break;
	case Functions::opr:
	    switch(instruction.address) {
	    case 0:
		// RETURN to caller
		stackRegister = baseRegister - 1;
		programCounter = stack[stackRegister + 3];
		baseRegister = stack[stackRegister + 2];
		break;
	    case 1:
		// unary minus
		stack[stackRegister] = - stack[stackRegister];
		break;
	    case 2:
		// plus operator
		--stackRegister;
		stack[stackRegister] = stack[stackRegister] + stack[stackRegister + 1];
		break;
	    case 3:
		// minus operator
		--stackRegister;
		stack[stackRegister] = stack[stackRegister] - stack[stackRegister + 1];
		break;
	    case 4:
		// multiply operator
		--stackRegister;
		stack[stackRegister] = stack[stackRegister] * stack[stackRegister + 1];
		break;
	    case 5:
		// Divide operator
		--stackRegister;
		stack[stackRegister] = stack[stackRegister] / stack[stackRegister + 1];
		break;
	    case 6:
		// Odd operator
		stack[stackRegister] = ((stack[stackRegister] % 2 == 1) ? 1 : 0);
		break;
	    case 8:
		// eql operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] == stack[stackRegister + 1]);
		break;
	    case 9:
		// neq operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] != stack[stackRegister + 1]);
		break;
	    case 10:
		// lt operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] < stack[stackRegister + 1]);
		break;
	    case 11:
		// geq operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] >= stack[stackRegister + 1]);
		break;
	    case 12:
		// gt operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] > stack[stackRegister + 1]);
		break;
	    case 13:
		// leq operator
		--stackRegister;
		stack[stackRegister] = (stack[stackRegister] <= stack[stackRegister + 1]);
		break;
	    default:
		os << "Error: invalid val for OPR opcode " << instruction.address << "\n";
		break;
	    } // switch instruction.address)
	    break; // Took me a lot of debugging to find I'd left this one out. This break is for the outer switch :-)
	case Functions::lod:
	    ++stackRegister;
	    stack[stackRegister] = stack[baseAddress(instruction.level) + instruction.address];
	    break;
	case Functions::sto:
	    stack[baseAddress(instruction.level) + instruction.address] = stack[stackRegister];
	    os << stack[stackRegister] << "\n";
	    --stackRegister;
	    break;
	case Functions::cal:
	    // Setup a new stack frame:
	    stack[stackRegister + 1] = baseAddress(instruction.level); 
	    stack[stackRegister + 2] = baseRegister;
	    stack[stackRegister + 3] = programCounter;
	    baseRegister = stackRegister + 1;
	    programCounter = instruction.address;
	    break;
	case Functions::intr:
	    stackRegister += instruction.address;
	    break;
	case Functions::jmp:
	    programCounter = instruction.address;
	    break;
	case Functions::jpc:
	    if (stack[stackRegister] == 0) {
		programCounter = instruction.address;
	    }
	    --stackRegister;
	    break;
	default:
	    os << "ERROR: We have an invalid op code\n";
	    break;
	} 
    } while (programCounter != 0);
}
