#pragma once

#include <iostream>
#include "functions.h"

struct Instruction {
    Functions opCode;    // The actual op-code
    int level;   // level
    int address; // displacement address

    Instruction(Functions opCode, int level, int address);
};

std::ostream& operator <<(std::ostream& os, const Instruction& obj);
