#pragma once

#include <vector>
#include <iostream>
#include "instruction.h"
#include "functions.h"

using CodeArray = std::vector<Instruction>;

class CodeList {
 private:
    CodeArray code;
 public:
    void generate(Functions opCode, int level, int address);
    auto getLastIndex(void) { return !code.empty() ? code.size() - 1 : 0; } // Return index of last instruction pushed in 
    void listCode(std::ostream& os, CodeArray::size_type start, CodeArray::size_type end);
    void fixupAddress(CodeArray::size_type idx, int address);
    const CodeArray& getCodeArray(void) const;
};
