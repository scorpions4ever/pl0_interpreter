#pragma once

#include <string>
#include <sstream>
#include "scanner.h"
#include "symbols.h"

class Lexer {
 private:
    Scanner&       scanner;
    Symbols        tokenType;
    int            tokenValue;
    std::string    tokenName;
    char           ch;
    std::stringstream sstrm;
 public:
    Lexer(Scanner &scanner);
    bool getNextToken(void);
    auto getTokenType(void) { return tokenType; }
    auto getTokenValue(void) { return tokenValue; }
    auto getToken(void) { return tokenName; }
    auto getPosition(void) { return scanner.getCurrentPos(); }
};
