#include "symbols.h"

using namespace std::string_literals;

std::unordered_map<std::string, Symbols> symbols = {
    {"+"s, Symbols::plus},
    {"-"s, Symbols::minus},
    {"*"s, Symbols::times},
    {"/"s, Symbols::slash},
    {"("s, Symbols::lparen},
    {")"s, Symbols::rparen},
    {"="s, Symbols::eql},
    {","s, Symbols::comma},
    {"."s, Symbols::period},
    {"#"s, Symbols::neq},
    {"<"s, Symbols::lss},
    {">"s, Symbols::gtr},
    {"["s, Symbols::leq},
    {"]"s, Symbols::geq},
    {";"s, Symbols::semicolon},
};
