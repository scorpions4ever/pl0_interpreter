#pragma once

#include <unordered_set>
#include <vector>
#include <string>
#include <iostream>
#include "lexer.h"
#include "symbols.h"
#include "utilities.h"
#include "symtable.h"
#include "codelist.h"

class Parser {
 private:
    Lexer& lexer;
    SymTable symtable;
    CodeList codeList;
    void throwError(int errNum, const char *errmsg);
    void throwError(int errNum, const std::string errmsg);
    void doConstDeclaration(int level);
    void doVarDeclaration(int level, int* datasegIdx);
    void doStatement(int level);
    void doExpression(int level);
    void doCondition(int level);
    void doTerm(int level);
    void doFactor(int level);

 public:
    Parser(Lexer& lexer_obj);
    void doBlock(int level);
    void listCode(std::ostream &os);
    const CodeArray& getAssembledCode(void) const;
};

