#include <sstream>
#include "pl0exception.h"
#include "symtable.h"

using namespace std::string_literals;

void SymTable::addSymbol(std::string name, Objects kind, int level, int* address) {
    auto entry = SymtableEntry(name, kind, level, 0);
    if (kind == Objects::constant)
	; // essentially the above initialization
    else if (kind == Objects::variable) {
	entry = SymtableEntry(name, kind, level, *address);
	(*address)++;
    }
    else if (kind == Objects::procedure)
	entry = SymtableEntry(name, kind, level, *address);
    else 
	throw PL0Exception("Attempted to addSymbol() of unknown type"); // Cannot happen!
    table.push_back(entry);
}

long SymTable::findSymbol(std::string name) {
    // Find the item backwards
    long idx = static_cast<long>(table.size()) - 1;
    while (idx >= 0) {
	if (table[idx].name == name) {
	    return idx;
	}
	idx--;
    }

    return -1;
}

SymtableEntry SymTable::get(long position) {
    if (position < table.size()) {
	return table[position];
    }
    else {
	std::stringstream errorMsg;
	errorMsg << "Internal error: Symtable position " << position << " cannot be resolved as symtable only has " << table.size() << "entries";
	throw PL0Exception(errorMsg.str());
    }
}
