#include "codelist.h"

void CodeList::generate(Functions opCode, int level, int address) {
    code.push_back(Instruction(opCode, level, address));
}

void CodeList::listCode(std::ostream& os, CodeArray::size_type start, CodeArray::size_type end) {
    for (auto idx = start; idx <= end; idx++) {
	auto instruction = code[idx];
	os << idx << "\t" << instruction << "\n";
    }
}

void CodeList::fixupAddress(CodeArray::size_type idx, int address) {
    code[idx].address = address;
}

const CodeArray& CodeList::getCodeArray() const {
    return code;
}


