#pragma once

// This class is to fix bugs in certain C++14 compilers, specifically clang using gcc's libstdc++ 4.9.3 
// or before, which doesn't let constructs with Enum Classes be used as a key. E.g. on these compilers,
// you can't declare std::unordered_map<EnumClassType, std::string> foo or std::unordered_set<EnumClassType>bar, but
// with this workaround, you can do std::unordered_map<EnumClassType, std::string, EnumClassHash>

struct EnumClassHash {
    template <typename T>
    std::size_t operator()(T const& t) const {
	return static_cast<std::size_t>(t);
    }
};
