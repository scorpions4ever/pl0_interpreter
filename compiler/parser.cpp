#include <sstream>
#include <string>
#include "parser.h"
#include "functions.h"
#include "pl0exception.h"

using namespace std::string_literals;

Parser::Parser(Lexer& lexer_obj) : lexer(lexer_obj) {

}

void Parser::throwError(int errNum, const char *msg) {
    std::stringstream errorstrm;
    errorstrm << "Error (" << errNum << "): " << msg << " at position "
	      << lexer.getPosition();
    
    throw PL0Exception(errorstrm.str());

}

void Parser::throwError(int errNum, const std::string msg) {
    std::stringstream errorstrm;
    errorstrm << "Error (" << errNum << "): " << msg << " at position "
	      << lexer.getPosition();
    
    throw PL0Exception(errorstrm.str());

}

void Parser::doBlock(int level) {
    auto datasegIdx = 3; // Leave space for return address + stack ptr
    auto codeIdx = codeList.getLastIndex();
    codeList.generate(Functions::jmp, 0, 0); // We will fix up the jmp address later, which is why we inited codeIdx above:

    if (lexer.getTokenType() == Symbols::constsym) {
	lexer.getNextToken();
	doConstDeclaration(level);
	while (lexer.getTokenType() == Symbols::comma) {
	   lexer.getNextToken();
	   doConstDeclaration(level);
	}
	
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
    }

    if (lexer.getTokenType() == Symbols::varsym) {
	lexer.getNextToken();
	doVarDeclaration(level, &datasegIdx);
	while (lexer.getTokenType() == Symbols::comma) {
	   lexer.getNextToken();
	   doVarDeclaration(level, &datasegIdx);
	}
	
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}	
    }

    while ( (lexer.getTokenType() == Symbols::procsym)) {
	lexer.getNextToken();
	if (lexer.getTokenType() == Symbols::ident) {
	    auto address = static_cast<int>(codeList.getLastIndex());
	    symtable.addSymbol(lexer.getToken(), Objects::procedure, level, &address);
	    lexer.getNextToken();
	}
	else {
	    throwError(4, "invalid token after 'procedure' keyword");
	}

	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
	doBlock(level + 1); 
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
    }

    if (level == 0) {
	codeList.fixupAddress(codeIdx, codeList.getLastIndex() + 1);
    }
    else { 
	codeList.fixupAddress(codeIdx + 1, codeList.getLastIndex() + 1);
    }
    codeList.generate(Functions::intr, 0, datasegIdx);
    doStatement(level);
    codeList.generate(Functions::opr, 0, 0); // return operation

    //codeList.listCode(std::cout, codeIdx, codeList.getLastIndex() - 1);
}

void Parser::doConstDeclaration(int level) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto tokenName = lexer.getToken();
	lexer.getNextToken();
	if (lexer.getTokenType() == Symbols::eql) {
	    lexer.getNextToken();
	    if (lexer.getTokenType() == Symbols::number) {
		symtable.addSymbol(tokenName, Objects::constant, lexer.getTokenValue(), 0);
		lexer.getNextToken();
	    }
	    else {
		throwError(2, "expected a number after '=' token");
	    }
	}
	else {
	    throwError(3, "expected '=' after the 'const' token. Found '"s + lexer.getToken() + "' instead"s);
	}
    }
    else {
	throwError(4, "expected an identifier after 'const' keyword");
    }
}

void Parser::doVarDeclaration(int level, int *datasegIdx) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto tokenName = lexer.getToken();
	symtable.addSymbol(tokenName, Objects::variable, level, datasegIdx);
	lexer.getNextToken();
    }
    else {
	throwError(4, "expected an identifier after 'var' keyword");
    }
}

void Parser::doStatement(int level) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto idx = symtable.findSymbol(lexer.getToken());
	if (idx == -1) {
	    throwError(1, "identifier '"s + lexer.getToken() + "' was not declared as a var"s);
	}
	else {
	    if (symtable.get(idx).kind != Objects::variable) {
		throwError(12, "identifier '"s + lexer.getToken() + "' is not a variable"s);
	    }
	    auto varName = lexer.getToken();
	    lexer.getNextToken();
	    if (lexer.getTokenType() == Symbols::becomes) {
		lexer.getNextToken();
	    }
	    else {
		throwError(13, "expected ':=' after identifier '"s + varName + "'."s);
	    }
	    doExpression(level);
	    if (idx != -1) {
		codeList.generate(Functions::sto, level - symtable.get(idx).level, symtable.get(idx).address);
	    }
	}
    }
    else if (lexer.getTokenType() == Symbols::callsym) {
	lexer.getNextToken();
	if (lexer.getTokenType() != Symbols::ident) {
	    throwError(14, "expected a procedure name after 'call' keyword");
	}
	else {
	    auto idx = symtable.findSymbol(lexer.getToken());
	    if (idx == -1) {
		throwError(11, "identifier '"s + lexer.getToken() + "' was not declared as a procedure"s);
	    }
	    else {
		if (symtable.get(idx).kind == Objects::procedure) {
		    codeList.generate(Functions::cal, level - symtable.get(idx).level, symtable.get(idx).address + 1);
		}
		else {
		    throwError(15, "identifier '"s + lexer.getToken() + "' is not a procedure"s);
		}
		lexer.getNextToken();
	    }
	}
    }
    else if (lexer.getTokenType() == Symbols::ifsym) {
	lexer.getNextToken();
	doCondition(level);
	if (lexer.getTokenType() == Symbols::thensym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(16, "missing 'then' keyword");
	}
	auto codeIdx = codeList.getLastIndex();
	codeList.generate(Functions::jpc, 0, 0); /// Will fixup shortly
	doStatement(level);
	codeList.fixupAddress(codeIdx + 1, codeList.getLastIndex() + 1);
    }
    else if (lexer.getTokenType() == Symbols::beginsym) {
	lexer.getNextToken();
	doStatement(level);
	while (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	    doStatement(level);
	}
	if (lexer.getTokenType() == Symbols::endsym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(17, "keyword 'end' expected");
	}
    }
    else if (lexer.getTokenType() == Symbols::whilesym) {
	auto codeIdx1 = codeList.getLastIndex();
	lexer.getNextToken();
	doCondition(level);
	auto codeIdx2 = codeList.getLastIndex();
	codeList.generate(Functions::jpc, 0, 0);
	if (lexer.getTokenType() == Symbols::dosym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(18, "keyword 'do' expected");
	}
	doStatement(level);
	codeList.generate(Functions::jmp, 0, codeIdx1 + 1);
	codeList.fixupAddress(codeIdx2 + 1, codeList.getLastIndex() + 1);
    }
}

void Parser::doExpression(int level) {
    auto tokenType = lexer.getTokenType();
    if (tokenType == Symbols::plus || tokenType == Symbols::minus) {
	lexer.getNextToken();
	doTerm(level);
	if (tokenType == Symbols::minus) {
	    codeList.generate(Functions::opr, 0, 1);
	}
    }
    else {
	doTerm(level);
    }

    tokenType = lexer.getTokenType();
    while (tokenType == Symbols::plus || tokenType == Symbols::minus) {
	auto op = tokenType;
	lexer.getNextToken();
	tokenType = lexer.getTokenType();
	doTerm(level);
	if (op == Symbols::plus) {
	    codeList.generate(Functions::opr, 0, 2);
	}
	else {
	    codeList.generate(Functions::opr, 0, 3);
	}
    }
}

void Parser::doCondition(int level) {
    if (lexer.getTokenType() == Symbols::oddsym) {
	lexer.getNextToken();
	doExpression(level);
	codeList.generate(Functions::opr, 0, 6);
    }
    else {
	doExpression(level);
	auto tokenType = lexer.getTokenType();
	if ( (tokenType == Symbols::eql) || (tokenType == Symbols::neq) ||
	     (tokenType == Symbols::lss) || (tokenType == Symbols::leq) ||
	     (tokenType == Symbols::gtr) || (tokenType == Symbols::geq)) {
	    lexer.getNextToken();
	    doExpression(level);
	    switch (tokenType) {
	    case Symbols::eql:
		codeList.generate(Functions::opr, 0, 8);
		break;
	    case Symbols::neq:
		codeList.generate(Functions::opr, 0, 9);
		break;
	    case Symbols::lss:
		codeList.generate(Functions::opr, 0, 10);
		break;
	    case Symbols::geq:
		codeList.generate(Functions::opr, 0, 11);
		break;
	    case Symbols::gtr:
		codeList.generate(Functions::opr, 0, 12);
		break;
	    case Symbols::leq:
		codeList.generate(Functions::opr, 0, 13);
		break;
	    default:
		throw PL0Exception("We got a strange operator");
	    }
	}
	else {
	    throwError(20, "invalid token '"s + lexer.getToken() + "' found. Expected '<', '[', '=', '#', ']' or '>' here"s);
	}
    }
}

void Parser::doTerm(int level) {
    doFactor(level);
    auto tokenType = lexer.getTokenType();
    while (tokenType == Symbols::times || tokenType == Symbols::slash) {
	auto op = tokenType;
	lexer.getNextToken();
	tokenType = lexer.getTokenType();
	doFactor(level);
	if (op == Symbols::times) {
	    codeList.generate(Functions::opr, 0, 4);
	}
	else {
	    codeList.generate(Functions::opr, 0, 5);
	}
    }
}

void Parser::doFactor(int level) {
    auto tokenType = lexer.getTokenType();
    if (tokenType == Symbols::ident) {
	auto idx = symtable.findSymbol(lexer.getToken());
	if (idx == -1) {
	    throwError(11, "could not find '"s + lexer.getToken() + "' declared before"s);
	}
	else {
	    switch(symtable.get(idx).kind) {
	    case Objects::constant:
		codeList.generate(Functions::lit, 0, symtable.get(idx).level);
		break;
	    case Objects::variable:
		codeList.generate(Functions::lod, level - symtable.get(idx).level, symtable.get(idx).address);
		break;
	    case Objects::procedure:
		throwError(21, "'"s + lexer.getToken() + "' is not declared as a const or var"s);
		break;
	    }
	}
	
	lexer.getNextToken();
    }
    else if (tokenType == Symbols::number) {
	codeList.generate(Functions::lit, 0, lexer.getTokenValue());
	lexer.getNextToken();
    }
    else if (tokenType == Symbols::lparen) {
	lexer.getNextToken();
	doExpression(level);
	if (lexer.getTokenType() == Symbols::rparen) {
	    lexer.getNextToken();
	}
	else {
	    throwError(22, "expected ')' here");
	}
    }
    else {
	throwError(23, "expected identifier here,got '"s + lexer.getToken() + "' instead");
    }
}

void Parser::listCode(std::ostream& os) {
    codeList.listCode(os, 0, codeList.getLastIndex() - 1);
}

const CodeArray& Parser::getAssembledCode(void) const {
    return codeList.getCodeArray();
}
