#pragma once

#include <stdexcept>

class PL0Exception : public std::runtime_error {
 public:
 PL0Exception(const char *error) : std::runtime_error(error) {};
 PL0Exception(const std::string error) : std::runtime_error(error) {};
};

