#include "keywords.h"

using namespace std::string_literals;

std::unordered_map<std::string, Symbols> keywords = {
    {"begin"s, Symbols::beginsym},
    {"call"s, Symbols::callsym},
    {"const"s, Symbols::constsym},
    {"do"s, Symbols::dosym},
    {"end"s, Symbols::endsym},
    {"if"s, Symbols::ifsym},
    {"odd"s, Symbols::oddsym},
    {"procedure"s, Symbols::procsym},
    {"then"s, Symbols::thensym},
    {"var"s, Symbols::varsym},
    {"while"s, Symbols::whilesym},
};
