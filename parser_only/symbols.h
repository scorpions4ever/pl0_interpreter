#pragma once

#include <unordered_map>
#include <string>

enum class Symbols {
    nul, ident, number, plus, minus, times, slash, oddsym,
        eql, neq, lss, leq, gtr, geq, lparen, rparen, comma, semicolon,
        period, becomes, beginsym, endsym, ifsym, thensym,
        whilesym, dosym, callsym, constsym, varsym, procsym
};

extern std::unordered_map<std::string, Symbols> symbols;
