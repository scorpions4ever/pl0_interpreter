#pragma once

#include <vector>
#include <string>
#include "objects.h"

struct SymtableEntry {
 public:
    std::string name;
    Objects kind;
    SymtableEntry(std::string name, Objects kind) {
	this->name = name;
	this->kind = kind;
    }
};

class SymTable {
 private:
    std::vector<SymtableEntry> table;
 public:
    void addSymbol(std::string name, Objects kind);
    long findSymbol(std::string name);
    SymtableEntry get(long idx);
};
