#include <iostream>
#include <unordered_map>
#include <string>
#include <memory>
#include "../lexer.h"
#include "../scanner.h"
#include "../symbols.h"
#include "../pl0exception.h"
#include "../utilities.h"

void usage(void);
bool isKeyword(Symbols tokenType);
std::string getType(std::unordered_map<Symbols, std::string, EnumClassHash>& tokenTypes, Symbols tokenType);

using namespace std::string_literals;

int main(int argc, char **argv) {
    if (argc < 2) {
        usage();
        return -1;
    }

    std::unordered_map<Symbols, std::string, EnumClassHash> tokenTypes = {
        {Symbols::nul, "null"s}, {Symbols::ident, "identifier"s}, {Symbols::number, "number"s}, {Symbols::plus, "plus"s},
        {Symbols::minus, "minus"s}, {Symbols::times, "times"s}, {Symbols::slash, "slash"s}, {Symbols::oddsym, "odd"s},
        {Symbols::eql, "equal"s}, {Symbols::neq, "not equal"s}, {Symbols::lss, "less than"s}, {Symbols::leq, "less or equal"s},
        {Symbols::gtr, "greater than"s}, {Symbols::geq, "greater or equal"s}, {Symbols::lparen, "Left Paren"s}, {Symbols::rparen, "Right Paren"s},
        {Symbols::comma, "comma"s}, {Symbols::semicolon, "semicolon"s}, {Symbols::period, "period"s}, {Symbols::becomes, "assignment"s},
        {Symbols::beginsym, "begin block"s}, {Symbols::endsym, "end block"s}, {Symbols::ifsym, "<if> keyword"s}, {Symbols::thensym, "<then> keyword"s},
        {Symbols::whilesym, "<while> keyword"s}, {Symbols::dosym, "<do> keyword"s}, {Symbols::callsym, "<call> keyword"s}, {Symbols::constsym, "<const> keyword"s},
        {Symbols::varsym, "<var> keyword"s}, {Symbols::procsym, "<procedure> keyword"s},
    };

    auto scanner = std::make_unique<Scanner>(argv[1]);
    auto lexer = std::make_unique<Lexer>(*scanner);

    try {
        while (lexer->getNextToken()) {
            auto tokenType = lexer->getTokenType();
            if (tokenType != Symbols::nul) {
                if (tokenType == Symbols::ident) {
                    std::cout << "IDENTIFER\t\t\t" << lexer->getToken() << "\n";
                }
                else if (isKeyword(tokenType)) {
                    std::cout << "KEYWORD\t" << getType(tokenTypes, tokenType) << "\t" << lexer->getToken() << "\n";
                }
                else if (tokenType == Symbols::number) {
                    std::cout << "NUMBER\t\t\t" << lexer->getTokenValue() << "\n";
                }
                else {
                    std::cout << "SYMBOL\t" << getType(tokenTypes, tokenType) << "\t" << lexer->getToken() << "\n";
                }
                if (tokenType == Symbols::period) {
                    std::cout << "\n\nReached end of code\n\n";
                    break;
                }
            }
            else {
                std::cout << "Illegal symbol \n";
                break;
            }
        }
    }
    catch (PL0Exception& e) {
        std::cerr << "Caught exception: " << e.what() << "\n\n";
    }

    return 0;
}

void usage(void) {
    std::cerr << "Usage: ./testlexer filename.pl0\n"
              << "Where filename.pl0 is a pl0 source file\n";
}

std::string getType(std::unordered_map<Symbols, std::string, EnumClassHash>& tokenTypes, Symbols tokenType) {
    auto tmp = tokenTypes.find(tokenType);
    if (tmp != tokenTypes.end()) {
        return tmp->second;
    }
    else {
        return std::string("<<<Unknown token type>>>");
    }
}

bool isKeyword(Symbols tokenType) {
    if ( (tokenType == Symbols::beginsym) || (tokenType == Symbols::endsym) ||
         (tokenType == Symbols::ifsym) || (tokenType == Symbols::thensym) ||
         (tokenType == Symbols::whilesym) || (tokenType == Symbols::dosym) ||
         (tokenType == Symbols::callsym) || (tokenType == Symbols::constsym) ||
         (tokenType == Symbols::varsym) || (tokenType == Symbols::procsym) ) {
        return true;
    }
    else {
        return false;
    }
}
