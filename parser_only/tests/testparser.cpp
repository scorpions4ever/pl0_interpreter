#include <iostream>
#include <memory>
#include "../parser.h"
#include "../lexer.h"
#include "../scanner.h"
#include "../symbols.h"
#include "../pl0exception.h"
#include "../utilities.h"

void usage(void);

int main(int argc, char **argv) {
    if (argc < 2) {
        usage();
        return -1;
    }

    auto scanner = std::make_unique<Scanner>(argv[1]);
    auto lexer = std::make_unique<Lexer>(*scanner);
    
    try {
	auto parser = std::make_unique<Parser>(*lexer);
	lexer->getNextToken(); // prime the pump here...
	parser->doBlock();
	std::cout << argv[1] << " is a valid pl0 program\n\n";
    }
    catch (PL0Exception& e) {
        std::cerr << "Caught exception: " << e.what() << "\n\n";
    }

    return 0;
}

void usage(void) {
    std::cerr << "Usage: ./testlexer filename.pl0\n"
              << "Where filename.pl0 is a pl0 source file\n";
}
