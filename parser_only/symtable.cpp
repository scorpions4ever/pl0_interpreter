#include <sstream>
#include "pl0exception.h"
#include "symtable.h"

void SymTable::addSymbol(std::string name, Objects kind) {
    SymtableEntry entry(name, kind);
    table.push_back(entry);
}

long SymTable::findSymbol(std::string name) {
    // Find the item backwards
    long idx = static_cast<long>(table.size()) - 1;
    while (idx >= 0) {
	if (table[idx].name == name) {
	    return idx;
	}
	idx--;
    }

    return -1;
}

SymtableEntry SymTable::get(long position) {
    if (position < table.size()) {
	return table[position];
    }
    else {
	std::stringstream errorMsg;
	errorMsg << "Internal error: Symtable position " << position << " cannot be resolved as symtable only has " << table.size() << "entries";
	throw PL0Exception(errorMsg.str());
    }
}
