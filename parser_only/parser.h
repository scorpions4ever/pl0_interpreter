#pragma once

#include <unordered_set>
#include <vector>
#include <string>
#include "lexer.h"
#include "symbols.h"
#include "utilities.h"
#include "symtable.h"

class Parser {
 private:
    Lexer& lexer;
    SymTable symtable;
    void throwError(int errNum, const char *errmsg);
    void throwError(int errNum, const std::string errmsg);
    void doConstDeclaration(void);
    void doVarDeclaration(void);
    void doStatement(void);
    void doExpression(void);
    void doCondition(void);
    void doTerm(void);
    void doFactor(void);

 public:
    Parser(Lexer& lexer_obj);
    void doBlock(void);
};

