#include <sstream>
#include <string>
#include "parser.h"
#include "pl0exception.h"

using namespace std::string_literals;

Parser::Parser(Lexer& lexer_obj) : lexer(lexer_obj) {

}

void Parser::throwError(int errNum, const char *msg) {
    std::stringstream errorstrm;
    errorstrm << "Error (" << errNum << "): " << msg << " at position "
	      << lexer.getPosition();
    
    throw PL0Exception(errorstrm.str());

}

void Parser::throwError(int errNum, const std::string msg) {
    std::stringstream errorstrm;
    errorstrm << "Error (" << errNum << "): " << msg << " at position "
	      << lexer.getPosition();
    
    throw PL0Exception(errorstrm.str());

}

void Parser::doBlock(void) {
    if (lexer.getTokenType() == Symbols::constsym) {
	lexer.getNextToken();
	doConstDeclaration();
	while (lexer.getTokenType() == Symbols::comma) {
	   lexer.getNextToken();
	   doConstDeclaration();
	}
	
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
    }

    if (lexer.getTokenType() == Symbols::varsym) {
	lexer.getNextToken();
	doVarDeclaration();
	while (lexer.getTokenType() == Symbols::comma) {
	   lexer.getNextToken();
	   doVarDeclaration();
	}
	
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}	
    }

    while ( (lexer.getTokenType() == Symbols::procsym)) {
	lexer.getNextToken();
	if (lexer.getTokenType() == Symbols::ident) {
	    symtable.addSymbol(lexer.getToken(), Objects::procedure);
	    lexer.getNextToken();
	}
	else {
	    throwError(4, "invalid token after 'procedure' keyword");
	}

	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
	doBlock(); 
	if (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	}
	else {
	    throwError(5, "missing semicolon");
	}
    }

    doStatement();
}

void Parser::doConstDeclaration(void) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto tokenName = lexer.getToken();
	lexer.getNextToken();
	if (lexer.getTokenType() == Symbols::eql) {
	    lexer.getNextToken();
	    if (lexer.getTokenType() == Symbols::number) {
		symtable.addSymbol(tokenName, Objects::constant);
		lexer.getNextToken();
	    }
	    else {
		throwError(2, "expected a number after '=' token");
	    }
	}
	else {
	    throwError(3, "expected '=' after the 'const' token. Found '"s + lexer.getToken() + "' instead"s);
	}
    }
    else {
	throwError(4, "expected an identifier after 'const' keyword");
    }
}

void Parser::doVarDeclaration(void) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto tokenName = lexer.getToken();
	symtable.addSymbol(tokenName, Objects::variable);
	lexer.getNextToken();
    }
    else {
	throwError(4, "expected an identifier after 'var' keyword");
    }
}

void Parser::doStatement(void) {
    if (lexer.getTokenType() == Symbols::ident) {
	auto idx = symtable.findSymbol(lexer.getToken());
	if (idx == -1) {
	    throwError(1, "identifier '"s + lexer.getToken() + "' was not declared as a var"s);
	}
	else {
	    if (symtable.get(idx).kind != Objects::variable) {
		throwError(12, "identifier '"s + lexer.getToken() + "' is not a variable"s);
	    }
	    auto varName = lexer.getToken();
	    lexer.getNextToken();
	    if (lexer.getTokenType() == Symbols::becomes) {
		lexer.getNextToken();
	    }
	    else {
		throwError(13, "expected ':=' after identifier '"s + varName + "'."s);
	    }
	    doExpression();
	}
    }
    else if (lexer.getTokenType() == Symbols::callsym) {
	lexer.getNextToken();
	if (lexer.getTokenType() != Symbols::ident) {
	    throwError(14, "expected a procedure name after 'call' keyword");
	}
	else {
	    auto idx = symtable.findSymbol(lexer.getToken());
	    if (idx == -1) {
		throwError(11, "identifier '"s + lexer.getToken() + "' was not declared as a procedure"s);
	    }
	    else {
		if (symtable.get(idx).kind != Objects::procedure) {
		    throwError(15, "identifier '"s + lexer.getToken() + "' is not a procedure"s);
		}
		lexer.getNextToken();
	    }
	}
    }
    else if (lexer.getTokenType() == Symbols::ifsym) {
	lexer.getNextToken();
	doCondition();
	if (lexer.getTokenType() == Symbols::thensym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(16, "missing 'then' keyword");
	}
	doStatement();
    }
    else if (lexer.getTokenType() == Symbols::beginsym) {
	lexer.getNextToken();
	doStatement();
	while (lexer.getTokenType() == Symbols::semicolon) {
	    lexer.getNextToken();
	    doStatement();
	}
	if (lexer.getTokenType() == Symbols::endsym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(17, "keyword 'end' expected");
	}
    }
    else if (lexer.getTokenType() == Symbols::whilesym) {
	lexer.getNextToken();
	doCondition();
	if (lexer.getTokenType() == Symbols::dosym) {
	    lexer.getNextToken();
	}
	else {
	    throwError(18, "keyword 'do' expected");
	}
	doStatement();
    }
}

void Parser::doExpression(void) {
    auto tokenType = lexer.getTokenType();
    if (tokenType == Symbols::plus || tokenType == Symbols::minus) {
	lexer.getNextToken();
	doTerm();
    }
    else {
	doTerm();
    }

    tokenType = lexer.getTokenType();
    while (tokenType == Symbols::plus || tokenType == Symbols::minus) {
	lexer.getNextToken();
	tokenType = lexer.getTokenType();
	doTerm();
    }
}

void Parser::doCondition(void) {
    if (lexer.getTokenType() == Symbols::oddsym) {
	lexer.getNextToken();
	doExpression();
    }
    else {
	doExpression();
	auto tokenType = lexer.getTokenType();
	if ( (tokenType == Symbols::eql) || (tokenType == Symbols::neq) ||
	     (tokenType == Symbols::lss) || (tokenType == Symbols::leq) ||
	     (tokenType == Symbols::gtr) || (tokenType == Symbols::geq)) {
	    lexer.getNextToken();
	    doExpression();
	}
	else {
	    throwError(20, "invalid token '"s + lexer.getToken() + "' found. Expected '<', '[', '=', '#', ']' or '>' here"s);
	}
    }
}

void Parser::doTerm(void) {
    doFactor();
    auto tokenType = lexer.getTokenType();
    while (tokenType == Symbols::times || tokenType == Symbols::slash) {
	lexer.getNextToken();
	tokenType = lexer.getTokenType();
	doFactor();
    }
}

void Parser::doFactor(void) {
    auto tokenType = lexer.getTokenType();
    if (tokenType == Symbols::ident) {
	auto idx = symtable.findSymbol(lexer.getToken());
	if (idx == -1) {
	    throwError(11, "could not find '"s + lexer.getToken() + "' declared before"s);
	}
	else if (symtable.get(idx).kind == Objects::procedure) {
	    throwError(21, "'"s + lexer.getToken() + "' is not declared as a const or var"s);
	}
	lexer.getNextToken();
    }
    else if (tokenType == Symbols::number) {
	lexer.getNextToken();
    }
    else if (tokenType == Symbols::lparen) {
	lexer.getNextToken();
	doExpression();
	if (lexer.getTokenType() == Symbols::rparen) {
	    lexer.getNextToken();
	}
	else {
	    throwError(22, "expected ')' here");
	}
    }
    else {
	throwError(23, "expected identifier here,got '"s + lexer.getToken() + "' instead");
    }
}
