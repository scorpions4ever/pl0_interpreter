#pragma once

#include <unordered_map>
#include <string>
#include "functions.h"
#include "utilities.h"

extern std::unordered_map<Functions, std::string, EnumClassHash> mnemonics;
