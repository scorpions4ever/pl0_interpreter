#include "mnemonics.h"

using namespace std::string_literals;

std::unordered_map<Functions, std::string, EnumClassHash> mnemonics = {
    {Functions::lit, "LIT"s},
    {Functions::opr, "OPR"s},
    {Functions::lod, "LOD"s},
    {Functions::sto, "STO"s},
    {Functions::cal, "CAL"s},
    {Functions::intr, "INT"s},
    {Functions::jmp, "JMP"s},
    {Functions::jpc, "JPC"s},
};
