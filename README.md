#PL0 Interpreter

by Mayukh Bose (mayukh_bose@hotmail.com)

## Language Implemented: C++ 14 (just because I can)

There are two sub-directories here: parser_only/ is just a parsing validator and compiler/ produces assembly code for a PL0 machine and executes it. It
might be good to search wikipedia for "pl/0 syntax" to see how the parser was built.

I do have a copy of _Algorithms + Data Structures = Programs_, but I didn't bother to implement a parser with error recovery (too boring!). By the way, 
my copy of the book was printed in the US in 1976 and on the copyright page, it says:

_Current Printing (last digit)_

19 18 17 16

So that is either the 19th printing or the 16th printing?

Either way, the code listing on page 336 has a bug on address 14. It reads:

14     OPR  7	odd

and it should be

14     OPR  6   odd

Code has been tested using the clang++ compiler on Mac OSX (Sierra) and OpenBSD 6.0 (more OSs to follow). Due to bugs in GNU libstdc++ in OpenBSD, you
will notice that std::unordered_map uses the 3 argument form in some places where the key is an "enum class" type. This is a known bug fixed in versions
greater than 4.9.3 of GNU libstdc++. Mac OSX doesn't have the same problem and can use the two argument form of declaration.
